What's this?
====================
This is my homework in the interview with the AmazingTalker.<br>
Although I received the thank you letter, I still had a lot of fun in this process and decided to make this project better.

Developer's note
----------------
- 2022/03/17<br>
  I will write local tests and it's important to understand the architecture of this app: <br>
  <img src = "architecture.png" width = 400 hight = 400>
    
    The following describes the functions of each class in order:
    - **AmazingTalkerService** - Get data from the network and convert data to data model.
  
    - **AmazingTalkerRepository** - Ask AmazingTalkerService to get data and handle result of this api call.

    - **ScheduleViewModel** - Ask data from AmazingTalkerRepository and use LiveData to represent the status of the data.<br>
        Expose methods to request new data.

    - **MainActivity** - Show a weekly calendar and schedules.<br> 
        Receive action from user and call the correspond method in ScheduleViewModel.

  The good news is that we built this app with Hilt so every class follows constructor injection,<br>
  we can simply call a class constructor by passing in fake or mock dependencies.<br>
  
  Now we can start building fake dependencies then test these classes🎆🎆🎆
