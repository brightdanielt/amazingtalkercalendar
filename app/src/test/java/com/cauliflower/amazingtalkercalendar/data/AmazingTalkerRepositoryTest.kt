package com.cauliflower.amazingtalkercalendar.data

import com.cauliflower.amazingtalkercalendar.network.FakeAmazingTalkerService
import com.cauliflower.amazingtalkercalendar.toScheduleList
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.core.IsEqual
import org.junit.Before
import org.junit.Test

@ExperimentalCoroutinesApi
class AmazingTalkerRepositoryTest {

    private lateinit var fakeAmazingTalkerService: FakeAmazingTalkerService
    private lateinit var amazingTalkerRepository: AmazingTalkerRepository

    @Before
    fun createRepository() {
        fakeAmazingTalkerService = FakeAmazingTalkerService()
        amazingTalkerRepository =
            AmazingTalkerRepository(fakeAmazingTalkerService, Dispatchers.Unconfined)
    }

    @Test
    fun testGetScheduleList() = runBlockingTest {
        val actualResult = amazingTalkerRepository.getScheduleList("") as NetworkResult.Success
        val expectScheduleList =
            fakeAmazingTalkerService.serverTimeTable.toScheduleList(Dispatchers.Unconfined)
        assertThat(actualResult.data, IsEqual(expectScheduleList))
    }

}