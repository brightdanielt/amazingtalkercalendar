package com.cauliflower.amazingtalkercalendar.data

import com.cauliflower.amazingtalkercalendar.CalendarUtils.toUtc
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime

class FakeAmazingTalkerRepository : BaseRepository {

    private val available = LessonPeriod(
        LocalTime.of(12, 0), LocalTime.of(12, 30), true
    )
    private val booked = LessonPeriod(
        LocalTime.of(12, 30), LocalTime.of(13, 0), false
    )
    private val periodList = arrayListOf(available, booked)
    private val todaySchedule = Schedule(LocalDate.now(), periodList)
    private val nextWeekSchedule = todaySchedule.copy().also { it.date.plusWeeks(1) }
    val thisWeekScheduleList = arrayListOf(todaySchedule)
    val nextWeekScheduleList = arrayListOf(nextWeekSchedule)

    override suspend fun getScheduleList(query: String): NetworkResult<List<Schedule>> {
        val queryDate = query.subSequence(0, 10)
        val todayUTC = toUtc(LocalDateTime.now()).subSequence(0, 10)
        return NetworkResult.Success(
            if (isSameDate(queryDate, todayUTC)) thisWeekScheduleList else nextWeekScheduleList
        )
    }

    private fun isSameDate(
        queryDate: CharSequence,
        nowUTC: CharSequence
    ) = queryDate == nowUTC
}