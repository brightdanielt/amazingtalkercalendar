package com.cauliflower.amazingtalkercalendar.network

import com.cauliflower.amazingtalkercalendar.data.DateTimeRange
import com.cauliflower.amazingtalkercalendar.data.ServerTimeTable
import retrofit2.Response

class FakeAmazingTalkerService : AmazingTalkerService {

    private val available = arrayListOf(
        DateTimeRange("2022-03-17T10:30:00Z", "2022-03-17T11:00:00Z")
    )
    private val booked = arrayListOf(
        DateTimeRange("2022-03-17T11:00:00Z", "2022-03-17T13:00:00Z")
    )

    val serverTimeTable = ServerTimeTable(available, booked)

    override suspend fun getTimeTable(query: String): Response<ServerTimeTable> {
        return Response.success(serverTimeTable)
    }
}