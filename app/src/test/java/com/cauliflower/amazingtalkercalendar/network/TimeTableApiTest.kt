package com.cauliflower.amazingtalkercalendar.network

import MockResponseFileReader
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.google.gson.GsonBuilder
import com.google.gson.JsonParser
import kotlinx.coroutines.runBlocking
import okhttp3.OkHttpClient
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.*
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

@RunWith(JUnit4::class)
class TimeTableApiTest {
    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    var server = MockWebServer()
    lateinit var mockedResponse: String
    lateinit var service: AmazingTalkerService

    private val gson = GsonBuilder().setLenient().create()

    @Before
    fun init() {
        server.start(8000)

        val baseUrl = server.url("/").toString()

        val okHttpClient = OkHttpClient
            .Builder()
            .build()
        service = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(baseUrl)
            .client(okHttpClient)
            .build().create(AmazingTalkerService::class.java)
    }


    @Test
    fun testApiSuccess() {
        mockedResponse = MockResponseFileReader("timeTableApi/success.json").content

        server.enqueue(
            MockResponse()
                .setResponseCode(200)
                .setBody(mockedResponse)
        )

        val response = runBlocking { service.getTimeTable("") }
        val json = gson.toJson(response.body())

        val resultResponse = JsonParser().parse(json)
        val expectedResponse = JsonParser().parse(mockedResponse)

        Assert.assertNotNull(response)
        Assert.assertTrue(resultResponse.equals(expectedResponse))
    }

    @After
    fun tearDown() {
        server.shutdown()
    }
}