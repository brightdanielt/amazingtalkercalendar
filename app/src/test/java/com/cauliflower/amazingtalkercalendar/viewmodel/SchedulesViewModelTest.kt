package com.cauliflower.amazingtalkercalendar.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.cauliflower.amazingtalkercalendar.MainCoroutineRule
import com.cauliflower.amazingtalkercalendar.data.FakeAmazingTalkerRepository
import com.cauliflower.amazingtalkercalendar.data.NetworkResult
import com.cauliflower.amazingtalkercalendar.getOrAwaitValue
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.core.IsEqual
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import java.time.LocalDate

@ExperimentalCoroutinesApi
class SchedulesViewModelTest {

    lateinit var amazingTalkerRepository: FakeAmazingTalkerRepository
    lateinit var schedulesViewModel: SchedulesViewModel

    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @Before
    fun createViewModel() {
        amazingTalkerRepository = FakeAmazingTalkerRepository()
        schedulesViewModel = SchedulesViewModel(amazingTalkerRepository)
    }

    @Test
    fun initialSelectedLocalDateTime() {
        assertThat(
            schedulesViewModel.selectedDateTime.getOrAwaitValue().toLocalDate(),
            IsEqual(LocalDate.now())
        )
    }

    @Test
    fun initialScheduleList() {
        val result = schedulesViewModel.scheduleList.value as NetworkResult.Success
        assertThat(result.data, IsEqual(amazingTalkerRepository.thisWeekScheduleList))
    }

    /**
     * Schedules before today are not available so the ScheduleList should still be this week's.
     * */
    @Test
    fun testPreviousWeekAction() {
        schedulesViewModel.previousWeekAction()
        val result = schedulesViewModel.scheduleList.value as NetworkResult.Success
        assertThat(result.data, IsEqual(amazingTalkerRepository.thisWeekScheduleList))
    }

    @Test
    fun testNextWeekAction() {
        schedulesViewModel.nextWeekAction()
        val result = schedulesViewModel.scheduleList.value as NetworkResult.Success
        assertThat(result.data, IsEqual(amazingTalkerRepository.nextWeekScheduleList))
    }
}