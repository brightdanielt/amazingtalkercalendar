package com.cauliflower.amazingtalkercalendar

import com.cauliflower.amazingtalkercalendar.CalendarUtils.daysInWeekArray
import com.cauliflower.amazingtalkercalendar.CalendarUtils.formattedTime
import com.cauliflower.amazingtalkercalendar.CalendarUtils.fromUtcToLocalDateTime
import com.cauliflower.amazingtalkercalendar.CalendarUtils.getGmtOffset
import com.cauliflower.amazingtalkercalendar.CalendarUtils.getNameOfTheTimeZone
import com.cauliflower.amazingtalkercalendar.CalendarUtils.monthYearFromDate
import com.cauliflower.amazingtalkercalendar.CalendarUtils.toUtc
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.core.IsEqual
import org.junit.After
import org.junit.Before
import org.junit.Test
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime
import java.util.*

class CalendarUtilsTest {
    private lateinit var defaultTimeZone: TimeZone
    private lateinit var defaultLocale: Locale
    lateinit var customTimeZone: TimeZone
    lateinit var customLocale: Locale

    @Before
    fun setup() {
        defaultTimeZone = TimeZone.getDefault()
        defaultLocale = Locale.getDefault()

        customTimeZone = TimeZone.getTimeZone("Asia/Taipei")
        customLocale = Locale.forLanguageTag("zh-Hant-TW")

        TimeZone.setDefault(customTimeZone)
        Locale.setDefault(customLocale)
    }

    @After
    fun tearDown() {
        TimeZone.setDefault(defaultTimeZone)
        Locale.setDefault(defaultLocale)
    }

    @Test
    fun testFormattedTime() {
        val time = LocalTime.of(13, 14)
        val formattedTime = formattedTime(time)
        assertThat(formattedTime, IsEqual("13:14"))
    }

    @Test
    fun fromUTCToLocalDateTime() {
        val actualLocalDateTime = fromUtcToLocalDateTime(
            dateTime = "2022-03-17T10:30:00Z"
        )
        val expectedLocalDateTime = LocalDateTime.of(
            2022, 3, 17, 10 + 8, 30, 0
        )
        assertThat(actualLocalDateTime, IsEqual(expectedLocalDateTime))
    }

    @Test
    fun testToUTC() {
        val localeDateTime = LocalDateTime.of(
            2022, 3, 17, 18, 30, 0
        )
        val actualDateTime = toUtc(localeDateTime)
        assertThat(actualDateTime, IsEqual("2022-03-17T10:30:00Z"))
    }

    @Test
    fun testMonthYearFromDate() {
        val date = LocalDate.of(2022, 3, 17)
        val actualMonthYear = monthYearFromDate(date, Locale.US)
        assertThat(actualMonthYear, IsEqual("March 2022"))
    }

    @Test
    fun testDaysInWeekArray() {
        val date = LocalDate.of(2022, 3, 17)
        val actualDays = daysInWeekArray(date)
        val expectedDays = arrayListOf(
            date.minusDays(4),
            date.minusDays(3),
            date.minusDays(2),
            date.minusDays(1),
            date,
            date.plusDays(1),
            date.plusDays(2)
        )
        assertThat(actualDays, IsEqual(expectedDays))
    }

    @Test
    fun testGetGMTOffSet() {
        val actualGmtOffSet = getGmtOffset()
        assertThat(actualGmtOffSet, IsEqual("GMT+08:00"))
    }

    @Test
    fun testGetNameOfTheTimeZone() {
        val actualName = getNameOfTheTimeZone()
        assertThat(actualName, IsEqual("台北標準時間"))
    }
}