package com.cauliflower.amazingtalkercalendar.data

import java.time.LocalDate
import java.time.LocalTime
import java.time.ZoneOffset

/**
 * 可被預約的時間和已被預約的時間
 * */
data class ServerTimeTable(
    val available: List<DateTimeRange>,
    val booked: List<DateTimeRange>
)

/**
 * The offset of the DateTime is [ZoneOffset.UTC].
 * */
data class DateTimeRange(
    var start: String,
    var end: String,
)

/**
 * @param date The date of this schedule.
 * @param periodList Store periods of lessons that belong to this date.
 * */
data class Schedule(
    val date: LocalDate,
    val periodList: ArrayList<LessonPeriod> = ArrayList()
)

/**
 * @param start  Start of a period of time.
 * @param end  End of a period of time.
 * @param isAvailable If this period of time is available, return true.
 * */
data class LessonPeriod(
    val start: LocalTime,
    val end: LocalTime,
    val isAvailable: Boolean
) : Comparable<LessonPeriod> {
    override fun compareTo(other: LessonPeriod): Int {
        return this.start.compareTo(other.start)
    }
}

