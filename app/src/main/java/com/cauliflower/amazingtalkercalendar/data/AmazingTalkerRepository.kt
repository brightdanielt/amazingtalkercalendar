package com.cauliflower.amazingtalkercalendar.data

import com.cauliflower.amazingtalkercalendar.network.AmazingTalkerService
import com.cauliflower.amazingtalkercalendar.safeApiCall
import com.cauliflower.amazingtalkercalendar.toScheduleList
import kotlinx.coroutines.CoroutineDispatcher

/**
 * This is a remote repository.
 * */
class AmazingTalkerRepository constructor(
    private val service: AmazingTalkerService,
    private val defaultDispatcher: CoroutineDispatcher
) : BaseRepository {
    override suspend fun getScheduleList(query: String): NetworkResult<List<Schedule>> {
        return when (val result = safeApiCall { service.getTimeTable(query) }) {
            is NetworkResult.Success -> {
                NetworkResult.Success(result.data.toScheduleList(defaultDispatcher))
            }
            is NetworkResult.Error -> NetworkResult.Error(result.exception)
            else -> NetworkResult.Loading
        }
    }
}