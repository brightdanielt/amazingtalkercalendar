package com.cauliflower.amazingtalkercalendar.data

interface BaseRepository {
    suspend fun getScheduleList(query: String): NetworkResult<List<Schedule>>
}