package com.cauliflower.amazingtalkercalendar

import com.cauliflower.amazingtalkercalendar.CalendarUtils.fromUtcToLocalDateTime
import com.cauliflower.amazingtalkercalendar.data.*
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import retrofit2.Response
import java.time.LocalDate

/**
 * Handle all of exceptions so the caller can get a reasonable result.
 * */
suspend fun <T : Any> BaseRepository.safeApiCall(call: suspend () -> Response<T>): NetworkResult<T> {
    val response: Response<T>
    try {
        response = call.invoke()
    } catch (t: Throwable) {
        return NetworkResult.Error(t)
    }
    return if (response.isSuccessful) {
        if (response.body() != null) {
            NetworkResult.Success(response.body()!!)
        } else {
            NetworkResult.Error(Throwable("Response's body can't be null"))
        }
    } else {
        NetworkResult.Error(
            Throwable(response.errorBody()?.toString() ?: "Empty error body")
        )
    }
}

/**
 * Compare if the two list are the same.
 * */
fun <T> List<T>.isEqual(another: List<T>): Boolean {
    if (this.size != another.size) {
        return false
    }
    return this.zip(another).all { (x, y) -> x == y }
}

/**
 * Combine the available list and booked list, locale the ZonedDateTime
 * and sort by DateTime from past to future.
 * */
suspend fun ServerTimeTable.toScheduleList(defaultDispatcher: CoroutineDispatcher): List<Schedule> {
    return withContext(defaultDispatcher) {
        val list = ArrayList<Schedule>()
        list.addTimeTables(available, true)
        list.addTimeTables(booked, false)
        list.forEach { it.periodList.sort() }
        list
    }
}

/**
 * Add the source UTC to dest in locale.
 * */
fun ArrayList<Schedule>.addTimeTables(
    source: List<DateTimeRange>,
    available: Boolean
) {
    for (range in source) {
        val localeDateTime = fromUtcToLocalDateTime(range.start)
        val sameDay = this.findOrAddSchedule(localeDateTime.toLocalDate())
        val startTime = localeDateTime.toLocalTime()
        val endTime = fromUtcToLocalDateTime(range.end).toLocalTime()
        val lessonPeriod = LessonPeriod(startTime, endTime, available)
        sameDay.periodList.add(lessonPeriod)
    }
}

/**
 * Find the schedule with same date, if there is no schedule with this date then add one.
 * */
fun ArrayList<Schedule>.findOrAddSchedule(date: LocalDate): Schedule {
    return findSchedule(date) ?: let {
        this.add(Schedule(date))
        this.last()
    }
}

/**
 * Find the schedule with same date.
 * */
fun List<Schedule>.findSchedule(date: LocalDate): Schedule? {
    for (schedule in this) {
        if (schedule.date.isEqual(date)) return schedule
    }
    return null
}
