package com.cauliflower.amazingtalkercalendar

import com.cauliflower.amazingtalkercalendar.data.ServerTimeTable
import com.google.gson.Gson
import java.text.SimpleDateFormat
import java.time.*
import java.time.format.DateTimeFormatter
import java.util.*

//Check below to see Patterns for Formatting and Parsing.
//https://docs.oracle.com/javase/8/docs/api/java/time/format/DateTimeFormatter.html
object CalendarUtils {
    /**
     * The pattern of DateTime from server comply with the ISO 8601
     * (https://en.wikipedia.org/wiki/ISO_8601),
     * you can use [Instant] to parse it.
     * */
    private const val patternDateTime = "yyyy-MM-dd'T'HH:mm:ssX"
    private const val patternTime = "HH:mm"

    fun formattedTime(time: LocalTime): String? {
        val formatter = DateTimeFormatter.ofPattern(patternTime)
        return time.format(formatter)
    }

    private fun fromUtcToZonedDateTime(dateTime: CharSequence, zoneId: ZoneId): ZonedDateTime {
        return Instant.parse(dateTime).atZone(zoneId)
    }

    fun fromUtcToLocalDateTime(
        dateTime: String,
        zoneId: ZoneId = ZoneId.systemDefault()
    ): LocalDateTime {
        val zoned = fromUtcToZonedDateTime(dateTime, zoneId)
        return zoned.toLocalDateTime()
    }

    fun toUtc(dateTime: LocalDateTime): String {
        val formatter = DateTimeFormatter.ofPattern(patternDateTime, Locale.getDefault())
        val zoned = ZonedDateTime.of(dateTime, ZoneId.systemDefault())
        val toOffsetDateTime = zoned.toOffsetDateTime()
        val withOffsetSameInstant = toOffsetDateTime.withOffsetSameInstant(ZoneOffset.UTC)
        val utc = withOffsetSameInstant.format(formatter)
        return utc
    }

    fun monthYearFromDate(date: LocalDate, locale: Locale = Locale.getDefault()): String? {
        val formatter = DateTimeFormatter.ofPattern("MMMM yyyy", locale)
        return date.format(formatter)
    }

    fun daysInWeekArray(selectedDate: LocalDate): ArrayList<LocalDate> {
        val days = ArrayList<LocalDate>()
        var current: LocalDate = sundayForDate(selectedDate)
        val endDate = current.plusWeeks(1)
        while (current.isBefore(endDate)) {
            days.add(current)
            current = current.plusDays(1)
        }
        return days
    }

    private fun sundayForDate(localDate: LocalDate): LocalDate {
        var current = localDate
        val oneWeekAgo = current.minusWeeks(1)
        while (current.isAfter(oneWeekAgo)) {
            if (current.dayOfWeek == DayOfWeek.SUNDAY) return current
            current = current.minusDays(1)
        }
        return current
    }

    fun getGmtOffset(locale: Locale = Locale.getDefault()): String {
        val calendar = Calendar.getInstance(TimeZone.getTimeZone("GMT"), locale)
        val timeZone = SimpleDateFormat("Z", locale).format(calendar.time)
        return "GMT" + timeZone.substring(0, 3) + ":" + timeZone.substring(3, 5)
    }

    fun getNameOfTheTimeZone(): String {
        return TimeZone.getDefault().displayName
    }

    fun fakeTimeTable(): ServerTimeTable {
        val json = "{\n" +
                "  \"available\": [\n" +
                "    {\n" +
                "      \"start\": \"2022-03-17T10:30:00Z\",\n" +
                "      \"end\": \"2022-03-17T11:00:00Z\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"start\": \"2022-03-17T13:00:00Z\",\n" +
                "      \"end\": \"2022-03-17T14:00:00Z\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"start\": \"2022-03-18T05:30:00Z\",\n" +
                "      \"end\": \"2022-03-18T07:00:00Z\"\n" +
                "    }\n" +
                "  ],\n" +
                "  \"booked\": [\n" +
                "    {\n" +
                "      \"start\": \"2022-03-17T11:00:00Z\",\n" +
                "      \"end\": \"2022-03-17T13:00:00Z\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"start\": \"2022-03-17T14:00:00Z\",\n" +
                "      \"end\": \"2022-03-17T15:00:00Z\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"start\": \"2022-03-18T07:00:00Z\",\n" +
                "      \"end\": \"2022-03-18T08:00:00Z\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"start\": \"2022-03-18T11:30:00Z\",\n" +
                "      \"end\": \"2022-03-18T13:00:00Z\"\n" +
                "    }\n" +
                "  ]\n" +
                "}"
        val table = Gson().fromJson(json, ServerTimeTable::class.java)
        return table
    }

}