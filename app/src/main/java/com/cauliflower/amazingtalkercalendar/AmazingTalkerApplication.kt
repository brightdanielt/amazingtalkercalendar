package com.cauliflower.amazingtalkercalendar

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class AmazingTalkerApplication : Application()