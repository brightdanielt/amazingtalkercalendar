package com.cauliflower.amazingtalkercalendar.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.cauliflower.amazingtalkercalendar.CalendarUtils.fakeTimeTable
import com.cauliflower.amazingtalkercalendar.CalendarUtils.toUtc
import com.cauliflower.amazingtalkercalendar.data.BaseRepository
import com.cauliflower.amazingtalkercalendar.data.NetworkResult
import com.cauliflower.amazingtalkercalendar.data.Schedule
import com.cauliflower.amazingtalkercalendar.toScheduleList
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.time.LocalDate
import java.time.LocalDateTime
import javax.inject.Inject

@HiltViewModel
class SchedulesViewModel @Inject constructor(
    private val repo: BaseRepository
) : ViewModel() {

    private val _selectedDateTime = MutableLiveData(LocalDateTime.now())
    val selectedDateTime: LiveData<LocalDateTime> = _selectedDateTime

    private val _scheduleList = MutableLiveData<NetworkResult<List<Schedule>>>()
    val scheduleList: LiveData<NetworkResult<List<Schedule>>> = _scheduleList

    init {
        querySchedules(_selectedDateTime.value!!)
//        fakeQuery(_selectedDateTime.value!!)
    }

    fun previousWeekAction() {
        if (_selectedDateTime.value!!.toLocalDate().isEqual(LocalDate.now())) {
            return
        }
        _selectedDateTime.value = _selectedDateTime.value!!.minusWeeks(1)
        querySchedules(_selectedDateTime.value!!)
//        fakeQuery(_selectedDateTime.value!!)
    }

    fun nextWeekAction() {
        _selectedDateTime.value = _selectedDateTime.value!!.plusWeeks(1)
        querySchedules(_selectedDateTime.value!!)
//        fakeQuery(_selectedDateTime.value!!)
    }

    /**
     * Query the timetable of the correspond one week or weeks,I am not sure.
     * You can also implement Paging if the server side support it.
     * */
    private fun querySchedules(date: LocalDateTime) {
        val query = toUtc(date)
        viewModelScope.launch {
            _scheduleList.value = NetworkResult.Loading
            when (val result = repo.getScheduleList(query)) {
                is NetworkResult.Success -> {
                    _scheduleList.value = result
                }
                is NetworkResult.Error -> {
                    //todo do actions depends on the error.
                }
                else -> {
                    //it is loading, do nothing
                }
            }
        }
    }

    private fun fakeQuery(date: LocalDateTime) {
        viewModelScope.launch {
            //Hard code the dispatcher to simplify this test.
            val scheduleList = fakeTimeTable().toScheduleList(Dispatchers.Default)
            _scheduleList.value = NetworkResult.Success(scheduleList)
        }
    }
}