package com.cauliflower.amazingtalkercalendar.network

import okhttp3.HttpUrl
import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response

/**
 * Interceptors can monitor, rewrite, and retry the API call.
 * */
class Interceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val original: Request = chain.request()
        val originalHttpUrl: HttpUrl = original.url

        /**
         * If there are other query params, add theme.
         * */
        val url = originalHttpUrl.newBuilder()
            .addQueryParameter("token", "value of token")
            .build()

        // Request customization: add request headers
        val requestBuilder: Request.Builder = original.newBuilder()
            .url(url)

        val request: Request = requestBuilder.build()
        return chain.proceed(request)
    }
}