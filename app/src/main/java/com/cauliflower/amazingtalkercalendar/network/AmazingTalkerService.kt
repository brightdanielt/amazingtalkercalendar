package com.cauliflower.amazingtalkercalendar.network

import com.cauliflower.amazingtalkercalendar.data.ServerTimeTable
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Using Retrofit to authenticating and interacting with APIs
 * and sending network requests with OkHttp.
 * */
interface AmazingTalkerService {
    /*
    * If there ary other queries, add them to the params.
    * */
    @GET("endPoint")
    suspend fun getTimeTable(@Query("query") query: String): Response<ServerTimeTable>
}