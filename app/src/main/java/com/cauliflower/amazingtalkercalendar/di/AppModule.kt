package com.cauliflower.amazingtalkercalendar.di

import com.cauliflower.amazingtalkercalendar.data.AmazingTalkerRepository
import com.cauliflower.amazingtalkercalendar.data.BaseRepository
import com.cauliflower.amazingtalkercalendar.network.AmazingTalkerService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.CoroutineDispatcher

@Module
@InstallIn(SingletonComponent::class)
class AppModule {

    @Provides
    fun providesAmazingTalkerRepository(
        service: AmazingTalkerService,
        @DefaultDispatcher dispatcher: CoroutineDispatcher
    ): BaseRepository {
        return AmazingTalkerRepository(service, dispatcher)
    }
}