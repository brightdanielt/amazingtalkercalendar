package com.cauliflower.amazingtalkercalendar.di

import androidx.viewbinding.BuildConfig
import com.cauliflower.amazingtalkercalendar.network.AmazingTalkerService
import com.cauliflower.amazingtalkercalendar.network.Interceptor
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

@InstallIn(SingletonComponent::class)
@Module
object NetworkModule {

    @Provides
    fun provideOkHttpClient(): OkHttpClient {
        return OkHttpClient.Builder().apply {
            addInterceptor(Interceptor())
            if (BuildConfig.DEBUG) {
                addInterceptor(
                    //Logs request and response lines
                    HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)
                )
            }
        }.build()
    }

    @Provides
    fun provideAmazingTalkerService(client: OkHttpClient): AmazingTalkerService {
        return Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            //todo update the baseUrl
            .baseUrl("https://api.amazingtalker")
            .client(client)
            .build()
            .create(AmazingTalkerService::class.java)
    }
}
