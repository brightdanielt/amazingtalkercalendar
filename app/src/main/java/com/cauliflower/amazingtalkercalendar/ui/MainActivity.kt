package com.cauliflower.amazingtalkercalendar.ui

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import com.cauliflower.amazingtalkercalendar.CalendarUtils.daysInWeekArray
import com.cauliflower.amazingtalkercalendar.CalendarUtils.getGmtOffset
import com.cauliflower.amazingtalkercalendar.CalendarUtils.getNameOfTheTimeZone
import com.cauliflower.amazingtalkercalendar.CalendarUtils.monthYearFromDate
import com.cauliflower.amazingtalkercalendar.R
import com.cauliflower.amazingtalkercalendar.data.NetworkResult
import com.cauliflower.amazingtalkercalendar.databinding.ActivityMainBinding
import com.cauliflower.amazingtalkercalendar.viewmodel.SchedulesViewModel
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import java.time.LocalDate

/**
 * In this page, you can see a weekly calendar and schedules that could
 * update depending on the week.
 * */
@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private val viewModel: SchedulesViewModel by viewModels()
    private lateinit var calendarAdapter: CalendarAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setListener()
        subscribeViewModel()
        showFriendlyReminder()
    }

    override fun onResume() {
        super.onResume()
        binding.tvTimeZoneHint.text = String.format(
            getString(R.string.time_zone_hint),
            getNameOfTheTimeZone(), getGmtOffset()
        )
    }

    private fun subscribeViewModel() {
        viewModel.scheduleList.observe(this) {
            when (it) {
                is NetworkResult.Success -> {
                    binding.loading.visibility = View.INVISIBLE
                    binding.rvDayInMonth.visibility = View.VISIBLE
                    calendarAdapter.swapScheduleList(it.data)
                }
                is NetworkResult.Error -> {
                    Toast.makeText(this, it.exception.message, Toast.LENGTH_LONG).show()
                    //todo do something
                }
                else -> {//Loading
                    binding.loading.visibility = View.VISIBLE
                    binding.rvDayInMonth.visibility = View.INVISIBLE
                }
            }
        }
        viewModel.selectedDateTime.observe(this) {
            setWeekView(it.toLocalDate())
        }
    }

    private fun setWeekView(date: LocalDate) {
        binding.tvMonthYear.text = monthYearFromDate(date)
        val days: ArrayList<LocalDate> = daysInWeekArray(date)
        calendarAdapter = CalendarAdapter(days)
        binding.rvDayInMonth.layoutManager = GridLayoutManager(applicationContext, 7)
        binding.rvDayInMonth.adapter = calendarAdapter
    }

    private fun setListener() {
        binding.btnPrevious.setOnClickListener { viewModel.previousWeekAction() }
        binding.btnNext.setOnClickListener { viewModel.nextWeekAction() }
    }

    private fun showFriendlyReminder() {
        Snackbar.make(
            binding.rvDayInMonth, "Go to 2022/3/17 to find data", Snackbar.LENGTH_LONG
        ).show()
    }

}