package com.cauliflower.amazingtalkercalendar.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.cauliflower.amazingtalkercalendar.CalendarUtils.formattedTime
import com.cauliflower.amazingtalkercalendar.R
import com.cauliflower.amazingtalkercalendar.data.LessonPeriod
import com.cauliflower.amazingtalkercalendar.databinding.ItemScheduleBinding
import java.time.LocalDate

/**
 * This adapter is responsible for showing the schedule.
 * which contains available time and booked time in one day
 * so the date of this adapter are the same.
 * */
class ScheduleAdapter(
    val date: LocalDate
) : ListAdapter<LessonPeriod, ScheduleAdapter.ScheduleViewHolder>(
    ScheduleDiffCallback()
) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ScheduleViewHolder {
        val binding = ItemScheduleBinding.inflate(
            LayoutInflater.from(parent.context), parent, false
        )
        return ScheduleViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ScheduleViewHolder, position: Int) {
        holder.bindView(getItem(position))
    }

    class ScheduleViewHolder(private val binding: ItemScheduleBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bindView(range: LessonPeriod) {
            val time = formattedTime(range.start)
            binding.tvTime.text = time
            setTextColor(range.isAvailable)
        }

        private fun setTextColor(available: Boolean) {
            val context = itemView.context
            val textColor = if (available) R.color.available else R.color.unavailable
            binding.tvTime.setTextColor(context.getColor(textColor))
        }
    }
}

class ScheduleDiffCallback : DiffUtil.ItemCallback<LessonPeriod>() {
    override fun areItemsTheSame(oldItem: LessonPeriod, newItem: LessonPeriod): Boolean {
        return oldItem.start == newItem.start && oldItem.end == newItem.end
    }

    override fun areContentsTheSame(oldItem: LessonPeriod, newItem: LessonPeriod): Boolean {
        return true
    }
}