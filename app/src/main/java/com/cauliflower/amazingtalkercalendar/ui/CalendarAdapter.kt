package com.cauliflower.amazingtalkercalendar.ui

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.cauliflower.amazingtalkercalendar.R
import com.cauliflower.amazingtalkercalendar.data.LessonPeriod
import com.cauliflower.amazingtalkercalendar.data.Schedule
import com.cauliflower.amazingtalkercalendar.databinding.ItemCalendarCellBinding
import com.cauliflower.amazingtalkercalendar.findSchedule
import java.time.LocalDate

/**
 * This adapter is responsible for showing 7 dayInMonth
 * and 7 recyclerview which show the schedules.
 * */
internal class CalendarAdapter(
    private val days: ArrayList<LocalDate>
) : RecyclerView.Adapter<CalendarAdapter.CalendarViewHolder>() {

    private var scheduleList: List<Schedule>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CalendarViewHolder {
        val binding = ItemCalendarCellBinding.inflate(
            LayoutInflater.from(parent.context), parent, false
        )
        return CalendarViewHolder(binding)
    }

    override fun onBindViewHolder(holder: CalendarViewHolder, position: Int) {
        val date = days[position]
        holder.setDayInMonth(date)

        //Do not show schedule if it's before.
        if (date.isBefore(LocalDate.now())) {
            return
        }
        scheduleList?.let { list ->
            val schedule = list.findSchedule(date)
            schedule?.let { holder.setSchedule(date, it.periodList) }
        }
    }

    override fun getItemCount(): Int {
        return days.size
    }

    @SuppressLint("NotifyDataSetChanged")
    fun swapScheduleList(list: List<Schedule>) {
        this.scheduleList = list
        notifyDataSetChanged()
    }

    class CalendarViewHolder(
        private val binding: ItemCalendarCellBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        private lateinit var adapter: ScheduleAdapter

        fun setDayInMonth(date: LocalDate) {
            binding.tvDayInMonth.text = date.dayOfMonth.toString()
            if (date.isBefore(LocalDate.now())) {
                binding.tvDayInMonth.setTextColor(itemView.context.getColor(R.color.unavailable))
            }
        }

        fun setSchedule(date: LocalDate, schedule: List<LessonPeriod>) {
            adapter = ScheduleAdapter(date)
            binding.rvSchedule.adapter = adapter
            adapter.submitList(schedule)
        }
    }

}