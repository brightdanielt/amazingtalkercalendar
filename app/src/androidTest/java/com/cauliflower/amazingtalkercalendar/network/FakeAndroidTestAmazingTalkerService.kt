package com.cauliflower.amazingtalkercalendar.network

import com.cauliflower.amazingtalkercalendar.CalendarUtils
import com.cauliflower.amazingtalkercalendar.data.DateTimeRange
import com.cauliflower.amazingtalkercalendar.data.ServerTimeTable
import retrofit2.Response
import java.time.Instant
import java.time.LocalDateTime
import java.time.temporal.ChronoUnit

/**
 * We cannot share test classes between the test and androidTest source sets by default.
 * So, we need to make a duplicate FakeTestAmazingTalkerService class in the androidTest source set.
 *
 * If you'd like to share files between the test and androidTest source sets, you can configure,
 * via gradle, a sharedTest folder as seen in the Architecture Blueprints reactive sample.
 * */
//https://github.com/android/architecture-samples/blob/f4128dd8dbea5d1aac5d5acd5f346bb82187fbe6/app/build.gradle#L20
class FakeAndroidTestAmazingTalkerService : AmazingTalkerService {

    private val now = Instant.now()
    private val nextWeek = now.plus(7, ChronoUnit.DAYS)
    private val tomorrow = Instant.now().plus(1, ChronoUnit.DAYS)
    private val available = arrayListOf(
        DateTimeRange(
            now.toString(),
            now.plus(30, ChronoUnit.MINUTES).toString()
        )
    )
    private val booked = arrayListOf(
        DateTimeRange(
            tomorrow.plus(30, ChronoUnit.MINUTES).toString(),
            tomorrow.plus(60, ChronoUnit.MINUTES).toString(),
        )
    )

    val thisWeekServerTimeTable = ServerTimeTable(available, booked)
    val nextWeekServerTimeTable = ServerTimeTable(
        arrayListOf(
            DateTimeRange(
                nextWeek.plus(60, ChronoUnit.MINUTES).toString(),
                nextWeek.plus(90, ChronoUnit.MINUTES).toString()
            )
        ), arrayListOf()
    )

    override suspend fun getTimeTable(query: String): Response<ServerTimeTable> {
        val queryDate = query.subSequence(0, 10)
        val todayUTC = CalendarUtils.toUtc(LocalDateTime.now()).subSequence(0, 10)
        return Response.success(
            if (isSameDate(queryDate, todayUTC))
                thisWeekServerTimeTable else nextWeekServerTimeTable
        )
    }
}

private fun isSameDate(
    queryDate: CharSequence,
    nowUTC: CharSequence
) = queryDate == nowUTC
