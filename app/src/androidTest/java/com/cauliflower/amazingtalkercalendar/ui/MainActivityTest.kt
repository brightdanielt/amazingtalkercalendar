package com.cauliflower.amazingtalkercalendar.ui

import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.cauliflower.amazingtalkercalendar.CalendarUtils.formattedTime
import com.cauliflower.amazingtalkercalendar.R
import com.cauliflower.amazingtalkercalendar.di.NetworkModule
import com.cauliflower.amazingtalkercalendar.network.AmazingTalkerService
import com.cauliflower.amazingtalkercalendar.network.FakeAndroidTestAmazingTalkerService
import com.cauliflower.amazingtalkercalendar.toScheduleList
import dagger.hilt.android.testing.BindValue
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import dagger.hilt.android.testing.UninstallModules
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@ExperimentalCoroutinesApi
@RunWith(AndroidJUnit4::class)
@UninstallModules(NetworkModule::class)
@HiltAndroidTest
class MainActivityTest {

    @get:Rule
    val hiltRule = HiltAndroidRule(this)

    /**
     * Replace the binding in a single test.
     * To inject with the fake dependency for all the tests in that folder, check android developer document.
     * */
    //https://developer.android.com/training/dependency-injection/hilt-testing#replace-binding
    @BindValue
    @JvmField
    val amazingTalkerService: AmazingTalkerService = FakeAndroidTestAmazingTalkerService()

    @Test
    fun default_thisWeekSchedules_displayedInUi() = runBlockingTest {
        ActivityScenario.launch(MainActivity::class.java)
        val serverTimeTable =
            (amazingTalkerService as FakeAndroidTestAmazingTalkerService).thisWeekServerTimeTable
        val scheduleList = serverTimeTable.toScheduleList(Dispatchers.Unconfined)
        for (schedule in scheduleList) {
            schedule.periodList.forEach {
                val formattedTime = formattedTime(it.start)
                onView(withText(formattedTime)).check(matches(isDisplayed()))
            }
        }
    }

    @Test
    fun clickNextWeek_nextWeekSchedules_displayedInUi() = runBlockingTest {
        ActivityScenario.launch(MainActivity::class.java)
        onView(withId(R.id.btnNext)).perform(click())
        val serverTimeTable =
            (amazingTalkerService as FakeAndroidTestAmazingTalkerService).nextWeekServerTimeTable
        val scheduleList = serverTimeTable.toScheduleList(Dispatchers.Unconfined)
        for (schedule in scheduleList) {
            schedule.periodList.forEach {
                val formattedTime = formattedTime(it.start)
                onView(withText(formattedTime)).check(matches(isDisplayed()))
            }
        }
    }

    @Test
    fun clickPrevWeek_thisWeekSchedules_displayedInUi() = runBlockingTest {
        ActivityScenario.launch(MainActivity::class.java)
        onView(withId(R.id.btnPrevious)).perform(click())
        val serverTimeTable =
            (amazingTalkerService as FakeAndroidTestAmazingTalkerService).thisWeekServerTimeTable
        val scheduleList = serverTimeTable.toScheduleList(Dispatchers.Unconfined)
        for (schedule in scheduleList) {
            schedule.periodList.forEach {
                val formattedTime = formattedTime(it.start)
                onView(withText(formattedTime)).check(matches(isDisplayed()))
            }
        }
    }
}