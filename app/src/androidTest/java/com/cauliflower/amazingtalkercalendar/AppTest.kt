package com.cauliflower.amazingtalkercalendar

import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.cauliflower.amazingtalkercalendar.ui.MainActivity
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@HiltAndroidTest
class AppTest {
    @get:Rule
    val hiltRule = HiltAndroidRule(this)

    @Test
    fun test() {
        ActivityScenario.launch(MainActivity::class.java)

        onView(withId(R.id.tvTimeZoneHint)).check(matches(isDisplayed()))

        onView(withId(R.id.btnPrevious)).perform(click())

        onView(withId(R.id.rvDayInMonth)).check(matches(isDisplayed()))
    }
}